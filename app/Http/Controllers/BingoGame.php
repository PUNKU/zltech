<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BingoNumber;
use App\Models\BingoCard;

class BingoGame extends Controller
{
    
    public function index(){
        
        $bingo_number = 'Not yet generated';

        $cards[0] = 'No cards generated';

        return view('home', compact('bingo_number', 'cards'));
    }
    
    //This function call randomly generates numbers and checks if the number already exists in the table
    public function callBingoNumber(){
        
        $randomNumber =   rand(1,75);

        $previousNumbers = BingoNumber::all();

        if (BingoNumber::where('number', $randomNumber)->exists()) {
            $randomNumber = 'Repeated number';
        }else{
            BingoNumber::create([
            
                'number' => $randomNumber, 
            ]);
        }

        $bingo_number = $randomNumber;

        $cards[0] = 'No cards generated';

        return view('home', compact('bingo_number', 'cards'));
    }

    //This function generates the Bingo Cards
    public function createBingoCard(){
        
        $bingo_number = 'Not yet generated';

        $randomNumber =   rand(1,75);

        BingoCard::create([
            'column_b_1' => rand(1,15),
            'column_b_2' => rand(1,15),
            'column_b_3' => rand(1,15),
            'column_b_4' => rand(1,15),
            'column_b_5' => rand(1,15),
            'column_i_1' => rand(16,30),
            'column_i_2' => rand(16,30),
            'column_i_3' => rand(16,30), 
            'column_i_4' => rand(16,30), 
            'column_i_5' => rand(16,30),  
            'column_n_1' => rand(31,45),
            'column_n_2' => rand(31,45),
            'column_n_4' => rand(31,45),
            'column_n_5' => rand(31,45), 
            'column_g_1' => rand(46,60),
            'column_g_2' => rand(46,60), 
            'column_g_3' => rand(46,60), 
            'column_g_4' => rand(46,60), 
            'column_g_5' => rand(46,60),  
            'column_o_1' => rand(61,75),
            'column_o_2' => rand(61,75),
            'column_o_3' => rand(61,75),
            'column_o_4' => rand(61,75),
            'column_o_5' => rand(61,75),
        ]);
        
        $cards = BingoCard::all();

        return view('home', compact('bingo_number', 'cards'));
    }

}
