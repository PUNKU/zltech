<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BingoCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bingo_cards', function (Blueprint $table) {
            $table->id();
            $table->integer('column_b_1');
            $table->integer('column_b_2');
            $table->integer('column_b_3');
            $table->integer('column_b_4');
            $table->integer('column_b_5');
            $table->integer('column_i_1');
            $table->integer('column_i_2');
            $table->integer('column_i_3');
            $table->integer('column_i_4');
            $table->integer('column_i_5');
            $table->integer('column_n_1');
            $table->integer('column_n_2');
            $table->string('column_n_3')->default('free');
            $table->integer('column_n_4');
            $table->integer('column_n_5');
            $table->integer('column_g_1');
            $table->integer('column_g_2');
            $table->integer('column_g_3');
            $table->integer('column_g_4');
            $table->integer('column_g_5');
            $table->integer('column_o_1');
            $table->integer('column_o_2');
            $table->integer('column_o_3');
            $table->integer('column_o_4');
            $table->integer('column_o_5');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
